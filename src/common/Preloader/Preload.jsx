import React from 'react';
import {CircularProgress} from "@material-ui/core";

const Preloader = (props) => {
    return  (
        <div>
            <CircularProgress />
        </div>
    )
}

export default Preloader;