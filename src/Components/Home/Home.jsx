import React from "react";
import {compose} from "redux";
import {withAuthRedirect} from "../../hoc/withAuthRedirect";
import {connect} from "react-redux";

const Home = (props) => {
    return (
        <div>
            <h1 className={"header"}>HOME</h1>
        </div>
    )
}



export default compose(withAuthRedirect, connect(null, null))(Home);