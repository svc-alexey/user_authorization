import React from "react";
import styles from './Login.module.css'
import {useFormik} from "formik";
import * as yup from 'yup';
import {Button, Paper, TextField} from "@material-ui/core";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faRocketchat} from '@fortawesome/free-brands-svg-icons';
import {useDispatch, useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import {fetchData} from "../../../redux/actions";

const validationSchema = yup.object({
    email: yup
        .string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    password: yup
        .string('Enter your email')
        .min(8, 'Password should be of minimum 8 characters length')
        .required('Password is required'),
})

const Login = () => {

    const dispatch = useDispatch();
    const isAuth = useSelector(state => state.auth.isAuthorized);


    const formik = useFormik({
        initialValues: {
            email: 'email@example.com',
            password: '12345678',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
        dispatch(fetchData(values));
        },
    });
    if (isAuth) {
        return (
            <Redirect to={'/home'}/>
        )
    }
    return (
        <Paper elevation={10} className={styles.paper}>
            <form onSubmit={formik.handleSubmit} className={styles.form}>
                <div className={styles.header}>
                    <p>ChatChat</p>
                    <FontAwesomeIcon icon={faRocketchat} size={"3x"} color={"#002984"}/>
                </div>
                <TextField
                    fullWidth
                    id="email"
                    label="Email"
                    variant="outlined"
                    name="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}/>
                <TextField
                    fullWidth
                    id="password"
                    label="Password"
                    variant="outlined"
                    name="password"
                    type={"password"}
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    helperText={formik.touched.password && formik.errors.password}/>
                <Button color="primary" variant="contained" fullWidth type="submit">
                    Sign in
                </Button>
            </form>
        </Paper>
    )
}

export default Login;