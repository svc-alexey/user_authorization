import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/messaging';
import 'firebase/functions';

export const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyB1eDQPkhEInxqYSWPEokH_99gnffV0sks",
    authDomain: "user-authorization-e2384.firebaseapp.com",
    projectId: "user-authorization-e2384",
    storageBucket: "user-authorization-e2384.appspot.com",
    messagingSenderId: "923715108010",
    appId: "1:923715108010:web:901c17b454abd1423fe4d3",
    measurementId: "G-5X7SQQVZ3Q"
})

export const rrfConfig = {
    userProfile: 'users',
    useFirestoreForProfile: true
}

firebase.firestore();


