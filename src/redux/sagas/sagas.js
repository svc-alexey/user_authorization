import {all, takeEvery} from 'redux-saga/effects'
import {FETCH_MESSAGES_REQUEST} from "../types";
import loginSaga from "./loginSaga";

export function* rootSaga() {
    yield all([
        yield takeEvery(FETCH_MESSAGES_REQUEST, loginSaga)
    ])
}

