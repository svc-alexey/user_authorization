import {firebaseApp} from "../firebase";
import {put} from 'redux-saga/effects';
import {FETCH_MESSAGES_SUCCESS} from "../types";

export default function* loginSaga(action) {
    try {
        firebaseApp
            .auth()
            .signInWithEmailAndPassword(action.payload.email, action.payload.password)
            .then(yield put({type: FETCH_MESSAGES_SUCCESS}))
    } catch (error) {
        console.log(error)
    }
}
