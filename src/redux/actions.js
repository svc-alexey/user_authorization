import {FETCH_MESSAGES_REQUEST} from "./types";


export const fetchData = (data) => ({type: FETCH_MESSAGES_REQUEST, payload: data});

