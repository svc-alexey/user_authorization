import {combineReducers} from "redux";
import { firebaseReducer } from 'react-redux-firebase';
import {firestoreReducer} from "redux-firestore";
import authReducer from "./authReducer";

const rootReducer = combineReducers({
    auth: authReducer,
    firebaseReducer,
    firestoreReducer
});

export default rootReducer;