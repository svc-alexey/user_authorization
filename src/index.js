import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import ChatApp from "./App";

ReactDOM.render(
    <React.StrictMode>
                <ChatApp/>
    </React.StrictMode>,
    document.getElementById('root')
);

reportWebVitals();
