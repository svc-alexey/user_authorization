import React from "react";
import './App.css';
import store from "./redux/configureStore";
import {Redirect, Route, Switch} from "react-router-dom";
import Login from './Components/LoginPage/Login/Login';
import Home from "./Components/Home/Home";
import Register from "./Components/LoginPage/Register/Register";
import {Provider} from "react-redux";
import {BrowserRouter as Router} from "react-router-dom";

function App() {
    return (
        <div className="App">
            <Switch>
                <Redirect exact from="/" to="/home"/>
                <Route exact path={"/home"} component={Home}/>
                <Route exact path={"/register"} component={Register}/>
                <Route exact path={"/signin"} component={Login}/>
                <Route path={'*'}>
                    <div>Oops, 404 not found</div>
                    <div>test fetch repo</div>
                </Route>
            </Switch>
        </div>
    );
}

const ChatApp = () => {
    return (
        <Provider store={store}>
            <Router>
                <App/>
            </Router>
        </Provider>
    )
}

export default ChatApp;
