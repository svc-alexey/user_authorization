module.exports = {
    env: {
        test: {
            presets: [
                "@babel/preset-env",
                "@babel/preset-flow",
                "@babel/preset-react",
            ],
            plugins: [
                "@babel/plugin-syntax-dynamic-import",
                "@babel/plugin-proposal-class-properties",
                "@babel/plugin-transform-runtime",
                "@babel/plugin-syntax-jsx"
            ],
        },
        production: {
            presets: [
                ["@babel/preset-env", {modules: false}],
                "@babel/preset-flow",
                "@babel/preset-react",
            ],
            plugins: [
                "@babel/plugin-syntax-dynamic-import",
                "@babel/plugin-proposal-class-properties",
                "@babel/plugin-transform-runtime",
                "@babel/plugin-syntax-jsx"
            ],
        },
        development: {
            presets: [
                ["@babel/preset-env", {modules: false}],
                "@babel/preset-flow",
                "@babel/preset-react",
            ],
            plugins: [
                "@babel/plugin-syntax-dynamic-import",
                "@babel/plugin-proposal-class-properties",
                "@babel/plugin-transform-runtime",
                "@babel/plugin-syntax-jsx"
            ],
        },
    },
};